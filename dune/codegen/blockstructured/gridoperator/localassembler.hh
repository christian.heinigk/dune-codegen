// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_CODEGEN_BLOCKSTRUCTURED_GRIDOPERATOR_LOCALASSEMBLER_HH
#define DUNE_CODEGEN_BLOCKSTRUCTURED_GRIDOPERATOR_LOCALASSEMBLER_HH

#include <dune/pdelab/gridoperator/default/localassembler.hh>
#include <dune/codegen/blockstructured/gridfunctionspace/localfunctionspace.hh>
#include <dune/codegen/blockstructured/gridfunctionspace/lfsindexcache.hh>

namespace Dune{
  namespace PDELab {
    namespace Blockstructured {

      template<typename GO, typename LOP, bool nonoverlapping_mode = false>
      class LocalAssembler :
          public Dune::PDELab::DefaultLocalAssembler<GO, LOP, nonoverlapping_mode> {
      public:

        using Base =  Dune::PDELab::DefaultLocalAssembler<GO, LOP, nonoverlapping_mode>;

        using CU = typename Base::CU;
        using CV = typename Base::CV;

        using LFSU = LocalFunctionSpace<typename Base::GFSU>;
        using LFSV = LocalFunctionSpace<typename Base::GFSV>;

        using LFSUCache = LFSIndexCache<LFSU, CU>;
        using LFSVCache = LFSIndexCache<LFSV, CV>;

        using LocalResidualAssemblerEngine = Dune::PDELab::DefaultLocalResidualAssemblerEngine<LocalAssembler>;
        using LocalJacobianApplyAssemblerEngine = Dune::PDELab::DefaultLocalJacobianApplyAssemblerEngine<LocalAssembler>;

        //! Constructor with empty constraints
        LocalAssembler(LOP &lop, std::shared_ptr<typename GO::BorderDOFExchanger> border_dof_exchanger)
            : Base(lop, border_dof_exchanger), residual_engine(*this), jacobian_apply_engine(*this) {}

        //! Constructor for non trivial constraints
        LocalAssembler(LOP &lop, const typename Base::CU &cu, const typename Base::CV &cv,
                       std::shared_ptr<typename GO::BorderDOFExchanger> border_dof_exchanger)
            : Base(lop, cu, cv, border_dof_exchanger), residual_engine(*this), jacobian_apply_engine(*this) {}

        //! Returns a reference to the requested engine. This engine is
        //! completely configured and ready to use.
        LocalResidualAssemblerEngine &localResidualAssemblerEngine
            (typename Base::Traits::Residual &r, const typename Base::Traits::Solution &x) {
          residual_engine.setResidual(r);
          residual_engine.setSolution(x);
          return residual_engine;
        }

        //! Returns a reference to the requested engine. This engine is
        //! completely configured and ready to use.
        LocalJacobianApplyAssemblerEngine &localJacobianApplyAssemblerEngine
            (const typename Base::Traits::Domain &update, typename Base::Traits::Range &result) {
          jacobian_apply_engine.setUpdate(update);
          jacobian_apply_engine.setResult(result);
          return jacobian_apply_engine;
        }

        //! Returns a reference to the requested engine. This engine is
        //! completely configured and ready to use.
        LocalJacobianApplyAssemblerEngine & localJacobianApplyAssemblerEngine
            (const typename Base::Traits::Domain & solution, const typename Base::Traits::Domain & update,
             typename Base::Traits::Range & result)
        {
          jacobian_apply_engine.setSolution(solution);
          jacobian_apply_engine.setUpdate(update);
          jacobian_apply_engine.setResult(result);
          return jacobian_apply_engine;
        }

      private:
        LocalResidualAssemblerEngine residual_engine;
        LocalJacobianApplyAssemblerEngine jacobian_apply_engine;
      };
    }
  }
}


#endif //DUNE_CODEGEN_BLOCKSTRUCTURED_GRIDOPERATOR_LOCALASSEMBLER_HH
