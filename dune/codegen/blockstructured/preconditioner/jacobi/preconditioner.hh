// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef NEUMANN_NEUMANN_PRECONDITIONER_PRECONDITIONER_HH
#define NEUMANN_NEUMANN_PRECONDITIONER_PRECONDITIONER_HH

#include <type_traits>
#include <dune/istl/preconditioner.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/solver.hh>
#include <dune/codegen/blockstructured/gridoperator/blockstructured.hh>


template<typename X, typename GOP, typename PointDiagonalLOP>
class JacobiPreconditioner : public Dune::Preconditioner<X, X>{
  using PointDiagonalGOP =  Dune::PDELab::BlockstructuredGridOperator<
      typename GOP::Traits::TrialGridFunctionSpace, typename GOP::Traits::TestGridFunctionSpace, PointDiagonalLOP,
      typename GOP::Traits::MatrixBackend, typename GOP::Traits::DomainField, typename GOP::Traits::RangeField,
      typename GOP::Traits::JacobianField>;

  static constexpr auto dummy_initializer(){
    if constexpr (GOP::LocalAssembler::isLinear())
      return 0.;
    else
      return Dune::PDELab::Backend::unattached_container{};
  }
public:
  JacobiPreconditioner(const GOP& gop_, PointDiagonalLOP& lop_)
      : gop(gop_), pd_lop(lop_), pd_gop(gop_.trialGridFunctionSpace(), gop_.testGridFunctionSpace(), lop_,
                                        gop_.matrixBackend()),
        dummy(gop_.trialGridFunctionSpace(), dummy_initializer()), u(&dummy),
        op(std::make_unique<Dune::PDELab::OnTheFlyOperator<X, X, GOP>>(gop))
  { }

  void setLinearizationPoint(const X& u_){
    u = &u_;
    op->setLinearizationPoint(u_);
  }

  void pre (X& x, X& b) override {
    X point_diagonal(pd_gop.trialGridFunctionSpace(), 0.);
    pd_gop.residual(*u, point_diagonal);
    inverse_point_diagonal = std::make_unique<DiagonalMatrixInverse<X>>(point_diagonal);
  }

  void apply (X& v, const X& d) override {
    static X r(d), u(d);
    for (int i = 0; i < 3; ++i) {
      r = d;
      op->applyscaleadd(-1., v, r);
      Dune::InverseOperatorResult res{};
      inverse_point_diagonal->apply(u, r, res);
      v += u;
    }
  }

  void post (X& x) override {}

  [[nodiscard]] Dune::SolverCategory::Category category() const override {
    return Dune::SolverCategory::sequential;
  }
private:
  const GOP& gop;
  PointDiagonalLOP& pd_lop;
  PointDiagonalGOP pd_gop;

  X dummy;
  const X* u;

  std::unique_ptr<Dune::PDELab::OnTheFlyOperator<X, X, GOP>> op;
  std::unique_ptr<DiagonalMatrixInverse<X>> inverse_point_diagonal;
};

#endif //NEUMANN_NEUMANN_PRECONDITIONER_PRECONDITIONER_HH
