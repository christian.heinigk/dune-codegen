#ifndef NEUMANN_NEUMANN_PRECONDITIONER_COARSE_GRID_CORRECTION_HH
#define NEUMANN_NEUMANN_PRECONDITIONER_COARSE_GRID_CORRECTION_HH

#include <dune/istl/preconditioner.hh>
#include <dune/istl/superlu.hh>
#include <dune/istl/solver.hh>
#include <dune/pdelab/backend/istl/bcrsmatrixbackend.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>

template<typename I, typename R, typename C_GOP, typename C_CC = Dune::PDELab::EmptyTransformation>
class CoarseGridCorrection : public Dune::Preconditioner<typename I::F_X,
                                                         typename I::F_X>{
  using DF = double;
  using RT = double;

  using F_X = typename I::F_X;
  using C_X = typename I::C_X;

  using X = F_X;

public:
  using backend = X;
  using native = typename Dune::PDELab::Backend::Native<X>;

  //! The type of the domain of the operator.
  typedef X domain_type;
  //! The type of the range of the operator.
  typedef X range_type;
  //! The field type of the operator.
  typedef typename X::field_type field_type;

  CoarseGridCorrection(const I& interpolation_, const R& restriction_, const C_GOP& c_gop_,
                       const C_CC& c_cc_ = C_CC(), std::shared_ptr<const C_X> c_dirichlet_values_ = nullptr)
      : interpolation(interpolation_), restriction(restriction_), c_gop(c_gop_), c_cc(c_cc_),
        c_dirichlet_values(c_dirichlet_values_), u(interpolation.coarse_space(), 0.),
        f_d(interpolation.fine_space()), c_d(interpolation.coarse_space()), f_v(interpolation.fine_space()),
        c_v(interpolation.coarse_space())
  { }

  void setLinearizationPoint(const F_X& u_){
    F_X copy(u_);
    u = 0.;
    if (c_dirichlet_values)
      Dune::PDELab::set_constrained_dofs(c_cc, 0., copy);
    restriction.apply(copy, u);
    if (c_dirichlet_values)
      Dune::PDELab::copy_constrained_dofs(c_cc, *c_dirichlet_values, u);
  }

  void pre (X& x, X& b) override {
    typename C_GOP::Jacobian jac(c_gop);
    c_gop.jacobian(u, jac);

    using Dune::PDELab::Backend::native;
    solver.setMatrix(native(jac));
  }

  void apply (X& v, const X& d) override {
    using Dune::PDELab::Backend::native;

    f_d = d;

    c_d = 0.;
    restriction.apply(f_d, c_d);

    c_v = 0.;
    Dune::InverseOperatorResult res;
    solver.apply(native(c_v), native(c_d), res);

    v = 0.;
    interpolation.apply(c_v, v);
  }

  void post (X& x) override {}

  [[nodiscard]] Dune::SolverCategory::Category category() const override { return Dune::SolverCategory::sequential; }

public:
  const I& interpolation;
  const R& restriction;

  const C_GOP& c_gop;

  const C_CC& c_cc;
  std::shared_ptr<const C_X> c_dirichlet_values;
  C_X u;

  F_X f_d;
  C_X c_d;
  F_X f_v;
  C_X c_v;

  Dune::SuperLU<Dune::BCRSMatrix<Dune::FieldMatrix<RT, 1, 1>>> solver;
};

#endif //NEUMANN_NEUMANN_PRECONDITIONER_COARSE_GRID_CORRECTION_HH
