from dune.codegen.generation import kernel_cached, valuearg, temporary_variable, instruction
from dune.codegen.options import get_form_option
from dune.codegen.blockstructured.tools import sub_element_inames, name_container_alias
from dune.codegen.pdelab.geometry import world_dimension
from dune.codegen.ufl.modified_terminals import Restriction
from loopy.types import NumpyType
import pymbolic.primitives as prim


# TODO remove the need for element
@kernel_cached
def pymbolic_coefficient(visitor, container, lfs, element, index):
    # TODO introduce a proper type for local function spaces!
    if isinstance(lfs, str):
        valuearg(lfs, dtype=NumpyType("str"))

    # If the LFS is not yet a pymbolic expression, make it one
    if not isinstance(lfs, prim.Expression):
        lfs = prim.Variable(lfs)

    # use higher order FEM index instead of Q1 index
    subelem_inames = sub_element_inames()
    coeff_alias = name_container_alias(container, lfs, element)

    if get_form_option("blockstructured_prioritize_quad_loop"):
        local_coeff = "{}_{}_loc".format(container, lfs.name)
        temporary_variable(local_coeff, shape=(element.degree() + 1,) * world_dimension(), managed=True)

        init_inames = visitor.lfs_inames(element, Restriction.NONE, number=None, context="local_init")

        instruction(assignee=prim.Subscript(prim.Variable(local_coeff), tuple(prim.Variable(i) for i in init_inames)),
                    expression=prim.Subscript(prim.Variable(coeff_alias),
                                              tuple(prim.Variable(i) for i in subelem_inames + init_inames)),
                    within_inames=frozenset(subelem_inames)
                    )

        aggregate = prim.Variable(local_coeff)
        indices = tuple(prim.Variable(i) if isinstance(i, str) else i for i in index)
    else:
        aggregate = prim.Variable(coeff_alias)
        indices = tuple(prim.Variable(i) if isinstance(i, str) else i for i in subelem_inames + index)

    return prim.Subscript(aggregate, indices)
