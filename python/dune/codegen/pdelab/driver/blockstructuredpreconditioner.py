from dune.codegen.generation import (include_file,
                                     class_member,
                                     preamble,
                                     get_global_context_value)
from dune.codegen.generation.cache import new_cache_context
from dune.codegen.options import (get_option,
                                  form_option_context,
                                  option_context,
                                  get_driverblock_option,
                                  get_form_option,
                                  get_current_driver_block)
from dune.codegen.pdelab.driver import (get_form,
                                        name_initree,
                                        FEM_name_mangling,
                                        get_trial_element)
from dune.codegen.pdelab.driver.constraints import (type_constraintscontainer,
                                                    name_assembled_constraints)
from dune.codegen.pdelab.driver.driverblock import (name_driver_block,
                                                    type_driver_block)
from dune.codegen.pdelab.driver.gridfunctionspace import (type_trial_gfs,
                                                          type_test_gfs,
                                                          name_trial_gfs,
                                                          name_leafview)
from dune.codegen.pdelab.driver.gridoperator import (type_coefficient_gfs,
                                                     type_gridoperator,
                                                     name_gridoperator,
                                                     name_localoperator,
                                                     type_localoperator)
from dune.codegen.pdelab.driver.solve import (type_vector,
                                              name_vector)


class BlockstructuredJacobiPreconditioner(object):
    def __init__(self, form_ident):
        self.form_ident = form_ident

        self.name = "pre_fine"
        self.lop = "point_diagonal_lop_{}".format(form_ident)

        ugfs = type_trial_gfs()
        vgfs = type_test_gfs()

        from dune.codegen.blockstructured.preconditioner import point_diagonal_form
        form = point_diagonal_form(get_form(form_ident))
        coefficients = sorted(filter(lambda c: c.count() > 2, form.coefficients()), key=lambda c: c.count())
        parameters = [ugfs, vgfs]
        if len(coefficients) > 0:
            for c in coefficients:
                parameters.append(type_coefficient_gfs(c))

        self.t_lop = "{}PointDiagonal<{}>".format(form_ident, ", ".join(parameters))
        self.type = "Jacobi"

        cmake_target = get_option("target_name")
        include_file('{}_{}PointDiagonal_file.hh'.format(cmake_target, form_ident), filetag="driver_block")

        self.declare()
        self.define()

    @class_member(classtag="driver_block")
    def declare(self):
        return ["using {} = JacobiPreconditioner<{}, {}, {}>;".format(self.type, type_vector(self.form_ident),
                                                                      type_gridoperator(self.form_ident), self.t_lop)] +\
               ["std::shared_ptr<{}> {};".format(n, t) for t, n in zip([self.name, self.lop], [self.type, self.t_lop])]

    @preamble(section="solver", kernel="driver_block")
    def define(self):
        gfs = name_trial_gfs()
        initree = name_initree()
        go = name_gridoperator(self.form_ident)
        code = ["{} = std::make_shared<{}>(*{}, *{}, {});".format(self.lop, self.t_lop, gfs, gfs, initree),
                "{} = std::make_shared<{}>(*{}, *{});".format(self.name, self.type, go, self.lop)]
        return code


class BlockstructuredSchurDecomposition(object):
    def __init__(self):
        self.name = "decomp"
        self.type = "Decomp"

        self.declare()
        self.define()

    @class_member(classtag="driver_block")
    def declare(self):
        return ["using {} = SchurDecomposition<{}, {}>;".format(self.type, type_trial_gfs(),
                                                                type_constraintscontainer()),
                "std::shared_ptr<{}> {};".format(self.type, self.name)]

    @preamble(section="solver", kernel="driver_block")
    def define(self):
        return "{} = std::make_shared<{}>(*{}, *{});".format(self.name, self.type, name_trial_gfs(),
                                                             name_assembled_constraints())


class BlockstructuredRestriction(object):
    def __init__(self, coarse_grid, decomp):
        self.name = "restriction"
        self.lop = "restriction_lop"

        element_name = FEM_name_mangling(get_trial_element())

        self.type = "Restriction"
        self.t_lop = "Restriction{}LocalOperator<{}, {}>".format(element_name, type_trial_gfs(), coarse_grid.t_gfs)

        cmake_target = get_option("target_name")
        include_file('{}_restriction_{}_operator_file.hh'.format(cmake_target, element_name), filetag="driver_block")

        self.declare(coarse_grid, decomp)
        self.define(coarse_grid, decomp)

    @class_member(classtag="driver_block")
    def declare(self, coarse_grid, decomp):
        return ["std::shared_ptr<{}> {};".format(self.t_lop, self.lop),
                "using {} = RestrictionOperator<{}, {}, {}, {}, {}, {}>;".format(
                    self.type, self.t_lop, type_trial_gfs(), coarse_grid.t_gfs, type_constraintscontainer(),
                    coarse_grid.t_cc, decomp.type),
                "std::shared_ptr<{}> {};".format(self.type, self.name)]

    @preamble(section="solver", kernel="driver_block")
    def define(self, coarse_grid, decomp):
        return ["{} = std::make_shared<{}>(*{}, *{}, {});".format(self.lop, self.t_lop, name_trial_gfs(),
                                                                  coarse_grid.gfs, name_initree()),
                "{} = std::make_shared<{}>(*{}, *{}, *{}, *{}, *{}, *{});".format(
                    self.name, self.type, self.lop, name_trial_gfs(), name_assembled_constraints(), coarse_grid.gfs,
                    coarse_grid.cc, decomp.name)]


class BlockstructuredInterpolation(object):
    def __init__(self, coarse_grid, decomp):
        self.name = "interpolation"
        self.lop = "interpolation_lop"

        coarse_space = get_global_context_value("data").object_by_name.get("coarse_space")
        with form_option_context(blockstructured=False):
            if coarse_space:
                element_name = FEM_name_mangling(coarse_space)
            else:
                element_name = FEM_name_mangling(get_trial_element())

        self.type = "Interpolation"
        self.t_lop = "Interpolation{}LocalOperator<{}, {}>".format(element_name, coarse_grid.t_gfs, type_trial_gfs())

        cmake_target = get_option("target_name")
        include_file('{}_interpolation_{}_operator_file.hh'.format(cmake_target, element_name), filetag="driver_block")

        self.declare(coarse_grid, decomp)
        self.define(coarse_grid, decomp)

    @class_member(classtag="driver_block")
    def declare(self, coarse_grid, decomp):
        return ["std::shared_ptr<{}> {};".format(self.t_lop, self.lop),
                "using {} = InterpolationOperator<{}, {}, {}, {}, {}, {}>;".format(
                    self.type, self.t_lop, type_trial_gfs(), coarse_grid.t_gfs, type_constraintscontainer(),
                    coarse_grid.t_cc, decomp.type),
                "std::shared_ptr<{}> {};".format(self.type, self.name)]

    @preamble(section="solver", kernel="driver_block")
    def define(self, coarse_grid, decomp):
        return ["{} = std::make_shared<{}>(*{}, *{}, {});".format(self.lop, self.t_lop, coarse_grid.gfs,
                                                                  name_trial_gfs(), name_initree()),
                "{} = std::make_shared<{}>(*{}, *{}, *{}, *{}, *{}, *{});".format(
                    self.name, self.type, self.lop, name_trial_gfs(), name_assembled_constraints(), coarse_grid.gfs,
                    coarse_grid.cc, decomp.name)]


class BlockstructuredCoarseGrid(object):
    def __init__(self, form_ident):
        self.coarse_form_ident = form_ident + "Coarse"

        from dune.codegen.blockstructured.preconditioner import transform
        form = get_form(form_ident)
        coarse_form = transform(form)

        data = get_global_context_value("data")
        data.object_by_name[form_ident] = coarse_form

        from dune.codegen.pdelab.localoperator import cgen_class_from_cache
        from dune.codegen.options import _driverblock_options
        current_db = get_current_driver_block()
        with new_cache_context():
            with option_context(driver_block_to_build="CoarseGrid"):
                _driverblock_options["CoarseGrid"] = _driverblock_options[current_db]
                setattr(_driverblock_options["CoarseGrid"], "classname", "DriverBlockCoarseGrid")
                self.name = name_driver_block()
                self.type = type_driver_block()

                with form_option_context(blockstructured=False, classname='{}CoarseOperator'.format(form_ident),
                                         filename="{}_{}CoarseOperator_file.hh".format(get_option("target_name"),
                                                                                       form_ident)):
                    self.gfs = self.name + "->" + name_trial_gfs()
                    self.cc = self.name + "->" + name_assembled_constraints()
                    self.lop = self.name + "->" + name_localoperator(form_ident)
                    self.gop = self.name + "->" + name_gridoperator(form_ident)
                    self.x = self.name + "->" + name_vector(form_ident)

                    self.t_gfs = "typename " + self.type + "::" + type_trial_gfs()
                    self.t_cc = "typename " + self.type + "::" + type_constraintscontainer()
                    self.t_lop = "typename " + self.type + "::" + type_localoperator(form_ident)
                    self.t_gop = "typename " + self.type + "::" + type_gridoperator(form_ident)
                    self.t_x = "typename " + self.type + "::" + type_vector(form_ident)

                db_class = cgen_class_from_cache("driver_block")

                filename = get_driverblock_option("filename").replace("driverblock", "coarse_grid_driverblock")
                from dune.codegen.file import generate_file
                generate_file(filename, "driver_block", [db_class, ], headerguard=True)
        include_file(filename, filetag="driver_block")

        data.object_by_name[form_ident] = form

        self.declare()
        self.define()

    @class_member(classtag="driver_block")
    def declare(self):
        return ["using CoarseGridDB = {};".format(self.type),
                "std::shared_ptr<CoarseGridDB> {};".format(self.name)]

    @preamble(section="solver", kernel="driver_block")
    def define(self):
        return "{} = std::make_shared<CoarseGridDB>({}, {});".format(self.name, name_leafview(), name_initree())
        pass


class BlockstructuredCGC(object):
    def __init__(self, form_ident):
        self.form_ident = form_ident

        self.name = "pre_coarse"
        self.type = "CGC"

        coarse_grid = BlockstructuredCoarseGrid(form_ident)
        decomp = BlockstructuredSchurDecomposition()

        restriction = BlockstructuredRestriction(coarse_grid, decomp)
        interpolation = BlockstructuredInterpolation(coarse_grid, decomp)

        self.declare(coarse_grid, restriction, interpolation)
        self.define(coarse_grid, restriction, interpolation)

    @class_member(classtag="driver_block")
    def declare(self, coarse_grid, restriction, interpolation):
        return ["using {} = CoarseGridCorrection<{}, {}, {}, {}>;".format(self.type,
                                                                          interpolation.type, restriction.type,
                                                                          coarse_grid.t_gop, coarse_grid.t_cc),
                "std::shared_ptr<{}> {};".format(self.type, self.name)]

    @preamble(section="solver", kernel="driver_block")
    def define(self, coarse_grid, restriction, interpolation):
        return ["{} = std::make_shared<{}>(*{}, *{}, *{}, *{}, {});".format(
            self.name, self.type, interpolation.name, restriction.name, coarse_grid.gop, coarse_grid.cc, coarse_grid.x
        )]


class BlockstructuredPreconditioner(object):
    def __init__(self, form_ident):
        self.form_ident = form_ident

        self.name = "pre"
        self.type = "PRE"

        include_file('dune/codegen/blockstructured/preconditioner/preconditioner.hh', filetag="driver_block")

        if get_form_option("blockstructured_preconditioner") == "jacobi":
            self.fine = BlockstructuredJacobiPreconditioner(form_ident)
        else:
            raise NotImplementedError()
        self.coarse = BlockstructuredCGC(form_ident)

        self.declare()
        self.define()

    @class_member(classtag="driver_block")
    def declare(self):
        vt = type_vector(self.form_ident)
        return ["using {} = AdditiveTwoLevel<{}, {}>;".format(self.type, vt, vt),
                "std::shared_ptr<{}> {};".format(self.type, self.name)]

    @preamble(section="solver", kernel="driver_block")
    def define(self):
        return "{} = std::make_shared<{}>(*({}), *({}));".format(self.name, self.type, self.fine.name, self.coarse.name)
