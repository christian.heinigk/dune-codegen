from dune.codegen.generation import (class_member,
                                     include_file,
                                     preamble,
                                     )
from dune.codegen.pdelab.driver.gridfunctionspace import (name_initree,
                                                          name_leafview,
                                                          name_test_gfs,
                                                          name_trial_gfs,
                                                          type_domainfield,
                                                          type_range,
                                                          type_test_gfs,
                                                          type_trial_gfs,
                                                          )
from dune.codegen.pdelab.driver.constraints import (name_assembled_constraints,
                                                    type_constraintscontainer,
                                                    )
from dune.codegen.pdelab.driver.gridoperator import (generate_localoperator_coefficient_functions,
                                                     name_matrixbackend,
                                                     type_coefficient_gfs,
                                                     type_matrixbackend,
                                                     )
from dune.codegen.error import CodegenError
from dune.codegen.options import (get_driverblock_ident,
                                  get_driverblock_option,
                                  get_form_option,
                                  )


def find_operator_identifier(identifier):
    assert (identifier in ["blockdiagonal", "blockoffdiagonal", "pointdiagonal"])

    # Extract the operator identifier for the matrix free preconditioner operators
    preconditioner_operators = get_driverblock_option("matrix_free_preconditioner_operators", get_driverblock_ident())
    preconditioner_operators = [i.strip() for i in preconditioner_operators.split(",")]

    # Find the correct operator
    identifier_option_dict = {
        "blockdiagonal": "block_preconditioner_diagonal",
        "blockoffdiagonal": "block_preconditioner_offdiagonal",
        "pointdiagonal": "block_preconditioner_pointdiagonal",
    }
    operator = ""
    for i in preconditioner_operators:
        if get_form_option(identifier_option_dict[identifier], i):
            if operator == "":
                operator = i
            else:
                raise RuntimeError("Your list of preconditioner operators can only contain one blockdiagonal/blockoffdiagonal/pointdiagonal operator.")

    return operator


# ========================================
# Solver Options for Jacobi Preconditioner
# ========================================

@class_member(classtag="driver_block")
def typedef_jacobi_solver_options(name):
    return "using {} = Dune::PDELab::BlockSolverOptions;".format(name)


def type_jacobi_solver_options():
    name = "BlockSolverOptions"
    typedef_jacobi_solver_options(name)
    return name


@class_member(classtag="driver_block")
def declare_jacobi_solver_options(name):
    jacobi_solver_options_type = type_jacobi_solver_options()
    return "std::shared_ptr<{}> {};".format(jacobi_solver_options_type, name)


@preamble(section="solver", kernel="driver_block")
def define_jacobi_solver_options(name):
    declare_jacobi_solver_options(name)
    jacobi_solver_options = type_jacobi_solver_options()
    return "{} = std::make_shared<{}>();".format(name, jacobi_solver_options)


def name_jacobi_solver_options():
    name = "solver_options"
    define_jacobi_solver_options(name)
    return name


# ======================================
# Solver Stats for Jacobi Preconditioner
# ======================================

@class_member(classtag="driver_block")
def typedef_solver_stat(name):
    return "using {} = Dune::PDELab::SolverStatistics<int>;".format(name)


def type_solver_stat():
    name = "SolverStat"
    typedef_solver_stat(name)
    return name


@class_member(classtag="driver_block")
def declare_solver_stat(name):
    solver_stat_type = type_solver_stat()
    return "std::shared_ptr<{}> {};".format(solver_stat_type, name)


@preamble(section="solver", kernel="driver_block")
def define_solver_stat(name):

    declare_solver_stat(name)
    solver_stat_type = type_solver_stat()
    grid_view = name_leafview()
    return "{} = std::make_shared<{}>({}.comm());".format(name, solver_stat_type, grid_view)


def name_solver_stat():
    name = "solver_stat"
    define_solver_stat(name)
    return name


# =============================
# Preconditioner Local Operator
# =============================

#
# Generate LOPs for Jacobi, SOR, blockdiagonal, blockoffdiagonal and pointdiagonal
#

@class_member(classtag="driver_block")
def typedef_preconditioner_localoperator(name, form_ident, identifier):
    if (identifier == "jacobi"):
        block_diagonal_lop = type_preconditioner_localoperator(form_ident, "blockdiagonal")
        point_diagonal_lop = type_preconditioner_localoperator(form_ident, "pointdiagonal")
        gfs = type_trial_gfs()
        domain_field = type_domainfield()
        iterative_solver = "Dune::BiCGSTABSolver"
        include_file("dune/pdelab/backend/istl/matrixfree/iterativeblockjacobipreconditioner.hh", filetag="driver_block")
        return "using {} = Dune::PDELab::IterativeBlockJacobiPreconditionerLocalOperator<{}, {}, {}, {}, {}>;".format(name,
                                                                                                                      block_diagonal_lop,
                                                                                                                      point_diagonal_lop,
                                                                                                                      gfs,
                                                                                                                      domain_field,
                                                                                                                      iterative_solver)
    elif (identifier == "blockdiagonal" or identifier == "pointdiagonal"):
        ugfs = type_trial_gfs()
        vgfs = type_test_gfs()
        filename = get_form_option("filename", find_operator_identifier(identifier))
        assert (filename is not None and filename != "")
        include_file(filename, filetag="driver_block")
        lopname = get_form_option("classname", find_operator_identifier(identifier))
        assert (lopname is not None and filename != "")

        from dune.codegen.pdelab.driver import get_form
        form = get_form(form_ident)
        coefficients = sorted(filter(lambda c: c.count() > 2, form.coefficients()), key=lambda c: c.count())
        if len(coefficients) == 0:
            return "using {} = {}<{}, {}>;".format(name, lopname, ugfs, vgfs)
        else:
            coefficient_gfss = []
            for c in coefficients:
                coefficient_gfss.append(type_coefficient_gfs(c))
            return "using {} = {}<{}, {}, {}>;".format(name, lopname, ugfs, vgfs, ",".join(coefficient_gfss))


def type_preconditioner_localoperator(form_ident, identifier):
    assert (identifier in ["jacobi", "sor", "blockdiagonal", "blockoffdiagonal", "pointdiagonal"])
    name = "{}PreconditionerLOP{}".format(identifier.upper(), form_ident.upper())
    typedef_preconditioner_localoperator(name, form_ident, identifier)
    return name


@class_member(classtag="driver_block")
def declare_preconditioner_localoperator(name, form_ident, identifier):
    loptype = type_preconditioner_localoperator(form_ident, identifier)
    return "std::shared_ptr<{}> {};".format(loptype, name)


@preamble(section="solver", kernel="driver_block")
def define_preconditioner_localoperator(name, form_ident, identifier):
    if (identifier == "jacobi"):
        generate_localoperator_coefficient_functions(name, form_ident)
        declare_preconditioner_localoperator(name, form_ident, identifier)
        lop_type = type_preconditioner_localoperator(form_ident, identifier)
        block_diagonal_lop = name_preconditioner_localoperator(form_ident, "blockdiagonal")
        point_diagonal_lop = name_preconditioner_localoperator(form_ident, "pointdiagonal")
        trial_gfs = name_trial_gfs()
        solver_stat = name_solver_stat()
        block_solver_options = name_jacobi_solver_options()
        verbosity = "2"
        return "{} = std::make_shared<{}>(*{}, *{}, *{}, *{}, *{}, {});".format(name,
                                                                                lop_type,
                                                                                block_diagonal_lop,
                                                                                point_diagonal_lop,
                                                                                trial_gfs,
                                                                                solver_stat,
                                                                                block_solver_options,
                                                                                verbosity)
    elif (identifier == "blockdiagonal" or identifier == "pointdiagonal"):
        generate_localoperator_coefficient_functions(name, form_ident)
        declare_preconditioner_localoperator(name, form_ident, identifier)
        trial_gfs = name_trial_gfs()
        test_gfs = name_test_gfs()
        lop_type = type_preconditioner_localoperator(form_ident, identifier)
        ini = name_initree()
        return "{} = std::make_shared<{}>(*{}, *{}, {});".format(name, lop_type, trial_gfs, test_gfs, ini)


def name_preconditioner_localoperator(form_ident, identifier):
    assert (identifier in ["jacobi", "sor", "blockdiagonal", "blockoffdiagonal", "pointdiagonal"])
    name = "{}_preconditioner_lop_{}".format(identifier, form_ident)
    define_preconditioner_localoperator(name, form_ident, identifier)
    return name


# ============================
# Preconditioner Grid Operator
# ============================

@class_member(classtag="driver_block")
def typedef_matrix_free_preconditioner_grid_operator(name, form_ident):
    ugfs = type_trial_gfs()
    vgfs = type_test_gfs()

    # TODO -> SOR only works with fastdg, do block Jacobi for now
    lop = type_preconditioner_localoperator(form_ident, "jacobi")

    cc = type_constraintscontainer()
    mb = type_matrixbackend()
    df = type_domainfield()
    r = type_range()
    if get_form_option("fastdg"):
        if not get_form_option("sumfact"):
            raise CodegenError("FastDGGridOperator is only implemented for sumfactorization.")
        include_file("dune/pdelab/gridoperator/fastdg.hh", filetag="driver_block")
        return "using {} = Dune::PDELab::FastDGGridOperator<{}, {}, {}, {}, {}, {}, {}, {}, {}>;".format(name, ugfs, vgfs, lop, mb, df, r, r, cc, cc)
    else:
        include_file("dune/pdelab/gridoperator/gridoperator.hh", filetag="driver_block")
        return "using {} = Dune::PDELab::GridOperator<{}, {}, {}, {}, {}, {}, {}, {}, {}>;".format(name, ugfs, vgfs, lop, mb, df, r, r, cc, cc)


def type_matrix_free_preconditioner_grid_operator(form_ident):
    name = "PreconditionerGridOperator{}".format(form_ident.upper())
    typedef_matrix_free_preconditioner_grid_operator(name, form_ident)
    return name


@class_member(classtag="driver_block")
def declare_matrix_free_preconditioner_grid_operator(name, form_ident):
    gotype = type_matrix_free_preconditioner_grid_operator(form_ident)
    return "std::shared_ptr<{}> {};".format(gotype, name)


@preamble(section="solver", kernel="driver_block")
def define_matrix_free_preconditioner_grid_operator(name, form_ident):
    declare_matrix_free_preconditioner_grid_operator(name, form_ident)
    gotype = type_matrix_free_preconditioner_grid_operator(form_ident)
    ugfs = name_trial_gfs()
    vgfs = name_test_gfs()
    if ugfs != vgfs:
        raise NotImplementedError("Non-Galerkin methods currently not supported!")
    cc = name_assembled_constraints()

    # TODO -> SOR only works with fastdg, do block Jacobi for now
    lop = name_preconditioner_localoperator(form_ident, "jacobi")

    mb = name_matrixbackend()
    return ["{} = std::make_shared<{}>(*{}, *{}, *{}, *{}, *{}, *{});".format(name, gotype, ugfs, cc, vgfs, cc, lop, mb),
            "std::cout << \"gfs with \" << {}->size() << \" dofs generated  \"<< std::endl;".format(ugfs),
            "std::cout << \"cc with \" << {}->size() << \" dofs generated  \"<< std::endl;".format(cc)]


def name_matrix_free_preconditioner_grid_operator(form_ident):
    name = "preconditonier_grid_operator_{}".format(form_ident)
    define_matrix_free_preconditioner_grid_operator(name, form_ident)
    return name
