// About: This program implements the fenics tutorial about coupling the Navier
// Stokes equation to a chemical reaction.
//
// This test the following dune-codegen features:
//
// - Forms that depend on additional Coefficients
// - Driver block generation
// - Adding additional information to the Coefficient codegen_cargo
//
// This tests the operator splitting and driver block generation in
// dune-codegen. The parameter T in the ini file are set in such a way that it
// only does one time step. This test does not check if the result is
// correct. It only tests generation and compilation of the executable.

#include "config.h"

#include "dune/pdelab.hh"

#include "dune/testtools/gridconstruction.hh"
#include "dune/codegen/vtkpredicate.hh"

#include "chemical_reaction_ns_driverblock_ns.hh"
#include "chemical_reaction_ns_driverblock_reaction.hh"

int main(int argc, char** argv){
  try
  {

    // Initialize basic stuff...
    Dune::MPIHelper& mpihelper = Dune::MPIHelper::instance(argc, argv);
    std::string iniFile = "chemical_reaction_ns.ini";
    Dune::ParameterTree initree;
    Dune::ParameterTreeParser::readINITree(iniFile, initree);
    using RangeType = double;
    double time = 0.0;

    // Setup grid (view)...
    using Grid = Dune::ALUGrid<2, 2, Dune::simplex, Dune::conforming>;
    using GV = Grid::LeafGridView;
    using DF = Grid::ctype;
    IniGridFactory<Grid> factory(initree);
    std::shared_ptr<Grid> grid = factory.getGrid();
    GV gv = grid->leafGridView();

    //==========================//
    // NS problem driver block //
    //=========================//
    DriverBlockNS<GV> nsDriverBlock(gv, initree);
    auto nsOneStepMethod = nsDriverBlock.getOneStepMethod();
    auto nsCoefficient = nsDriverBlock.getCoefficient();
    auto nsGridFunctionSpace = nsDriverBlock.getGridFunctionSpace();
    auto nsConstraintsContainer = nsDriverBlock.getConstraintsContainer();
    auto nsBoundaryConditionType = nsDriverBlock.getBoundaryConditionType();
    auto nsBoundaryGridFunction = nsDriverBlock.getBoundaryGridFunction();
    using NSCoefficient = typename DriverBlockNS<GV>::V_NS;
    using NSGridFunctionSpace = typename DriverBlockNS<GV>::CG2_dirichlet_GFS_POW2GFS_CG1_GFS;

    //===============================//
    // Reaction problem driver block //
    //===============================//
    DriverBlockReaction<GV> reactionDriverBlock(gv, initree);
    auto reactionOneStepMethod = reactionDriverBlock.getOneStepMethod();
    auto reactionCoefficient = reactionDriverBlock.getCoefficient();
    auto reactionGridFunctionSpace = reactionDriverBlock.getGridFunctionSpace();
    using ReactionCoefficient = typename DriverBlockReaction<GV>::V_REACTION;

    // Use velocity field of the solution of the Navier Stokes problem
    reactionDriverBlock.setCoefficient1(nsGridFunctionSpace, nsCoefficient);

    //===============//
    // Visualization //
    //===============//
    Dune::RefinementIntervals subint(initree.get<int>("vtk.subsamplinglevel", 2));
    using VTKWriter = Dune::SubsamplingVTKWriter<GV>;
    VTKWriter vtkwriter(gv, subint);
    std::string vtkfile = initree.get<std::string>("wrapper.vtkcompare.name", "output");
    using VTKSW = Dune::VTKSequenceWriter<GV>;
    VTKSW vtkSequenceWriter(std::make_shared<VTKWriter>(vtkwriter), vtkfile);
    CuttingPredicate predicate;
    Dune::PDELab::addSolutionToVTKWriter(vtkSequenceWriter, *nsGridFunctionSpace, *nsCoefficient, Dune::PDELab::vtk::defaultNameScheme(), predicate);
    Dune::PDELab::addSolutionToVTKWriter(vtkSequenceWriter, *reactionGridFunctionSpace, *reactionCoefficient, Dune::PDELab::vtk::defaultNameScheme(), predicate);
    vtkSequenceWriter.write(time, Dune::VTK::appendedraw);

    //===========//
    // Time loop //
    //===========//
    double T = initree.get<double>("instat.T", 1.0);
    double dt = initree.get<double>("instat.dt", 0.1);
    int step_number(0);int output_every_nth = initree.get<int>("instat.output_every_nth", 1);
    NSCoefficient newNSCoefficient(*nsCoefficient);
    ReactionCoefficient newReactionCoefficient(*reactionCoefficient);
    while (time<T-1e-8){
      // Assemble constraints for new time step
      nsDriverBlock.setTime(time+dt);
      Dune::PDELab::constraints(*nsBoundaryConditionType, *nsGridFunctionSpace, *nsConstraintsContainer);

      // Navier Stokes time step
      nsOneStepMethod->apply(time, dt, *nsCoefficient, *nsBoundaryGridFunction, newNSCoefficient);
      *nsCoefficient = newNSCoefficient;

      // Chemical reaction time step
      reactionOneStepMethod->apply(time, dt, *reactionCoefficient, newReactionCoefficient);
      *reactionCoefficient = newReactionCoefficient;

      // Accept new time step
      time += dt;
      vtkSequenceWriter.write(time, Dune::VTK::appendedraw);
    }

    return 0;
  }
  catch (Dune::Exception& e)
  {    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
  catch (std::exception& e)
  {    std::cerr << "Unknown exception thrown!" << std::endl;
    return 1;
  }
}
